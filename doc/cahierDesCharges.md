
# COVID-RUN

------------------------------------
## cahier des charges :
------------------------------------
* __Equipe__ :
Mr RONOT Jules *Chef de projet*

Mr HARBI Noureddine  

Mr MAAS Valentin  

* __plan de travail__:  

 Après l'identfication des besoins à partir du scénario de l'étude (voir le fichier Scénario) et la mise en place de notre diagramme __UML__, les classes suivantes ont été réalisées :
 1. __class Personnage__  
 c'est une class public avec un constructeur pour la création d'un *Personnage*.
 2. __class NonVaccine__  
  cette classe est extends de la *class Personnage*  
 3. __class NonVaccineFragile__  
 cette classe est extends de la *class NonVaccine*

 4. __class Vaccine__  
 cette classe est extends de la *class Personnage*  

 5. __class NonVaccineNonFragile__  
   cette classe est extends de la *class NonVaccine*  

 6. __class NonVaccineNonFragile__  
  cette classe est extends de la *class NonVaccine*  

 7. __class VaccineFragile__  
 cette classe est extends de la *class Vaccine*   

 8. __class VaccineNonFragile__
 cette classe est extends de la *class Vaccine*   

 9. __class Obstacles__
 C'est une class abstract   

 10. __class Vaccin__  

 cette classe est extends de la *class Obstacles*
 permettra au joueur de se faire __vacciné__ ce qui le rendront moins vulnérable.

 11. __class Masques__
 C'est une class abstract extends de la *class Obstacles*  

 12. __class FFP2__   
 Cette classe est extends de la *class Masques*  

 13. __class Chirurgical__  
 Cette classe est extends de la *class Masques*  
 
 14. __class Partie__  

 15. __class CovidRun__     
 Cette classe est notre *programme principal*
