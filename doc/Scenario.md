
# COVID-RUN 
* Un jeu d'aventure 
------------------------------------
1.	__Préambule__

On est en l’an 2020, notre planète est touchée par une pandémie viral appelée __COVID-19__ 
Cette maladie infectieuse est due au virus SARS-CoV-2.
L’infection à __SARS-CoV-2__ provoque principalement une maladie respiratoire allant de la forme modérée à la forme sévère pouvant aller jusqu’au __décès__, tandis que certaines personnes infectées par le virus ne développent jamais de symptômes.  
Le virus peut se propager lorsque de petites particules liquides sont expulsées par la bouche ou par le nez quand une personne infectée tousse, éternue, chante ou respire. Ces particules sont de différentes tailles, allant de grosses gouttelettes respiratoires à des aérosols plus petits.
Vous pouvez être infecté en inhalant le virus si vous vous trouvez à proximité d'une personne atteinte de la __COVID-19__, ou en touchant une surface contaminée puis vos yeux, votre nez ou votre bouche. Le virus se propage plus facilement en intérieur et dans les espaces bondés.

L’épidémie de coronavirus a conduit les pouvoirs publics à prendre une série de mesures destinées à protéger la population et à faciliter l’accès aux soins.

__Vaccination__ 
La vaccination réduit le risque de contracter et de transmettre le coronavirus. En vous vaccinant, vous pouvez donc vous protéger vous-même et protéger les personnes vulnérables au sein de votre famille, dans votre ménage ou sur votre lieu de travail. 
Le vaccin offre une protection très élevée contre les formes graves du COVID-1.

__Masque FFP2__ /__Masque chirurgical__

La norme européenne __FFP2__ garantit une filtration de __94%__ des particules présentes dans l'air. Sa durée de protection varie entre trois et huit heures, ce qui offre une protection plus longue que le masque chirurgical. Si les deux personnes portent __un masque chirurgical classique__, __le risque monte à 30%__ .

La stratégie pour limiter la propagation du virus s’appuie également sur le repérage précoce des symptômes, __la réalisation de tests__ et __l’isolement des malades__ ainsi que __des personnes ayant été en contact avec eux__.

__Dépistage et diagnostic__
Actuellement, le dépistage de l'infection par le coronavirus repose sur __la réalisation d’un test PCR__ ou __antigénique__. Ce test permet de préciser à un instant T si la personne est porteuse ou non de gènes du virus dans cette partie du corps.

__Isolement__ 
personnes ayant été en contact (cas contact): 
Isolement pendant 2 jours le temps de la réalisation du test.
l’isolement des malades (cas positif) : 
5 jours de confinement pour les personnes vaccinés
7 jours de confinement pour les personnes non vaccinés

__Personnes vulnérables ou personne fragile__ :
L’espérance de vie d’une personne vulnérable est faible. La vulnérabilité de la personne est définie par l’âge et les comorbidités.
------------------------------------
2.	COVID-RUN 

C’est un jeu d’aventure qui a été inespéré de la pandémie COVID-19, le joueur doit traverser une carte de plusieurs cases en essayant d’éviter les cases COVID. Pour gagner le joueur doit traverser la carte en essayant d’avoir le score le plus faible possible.
__Les cases COVID__ sont réparties sur la carte d’une manière aléatoire.
Si le joueur est déclaré cas contact, il doit s’isoler pendant deux jours et réaliser un test COVID.
En cas de test COVID positif le joueur doit s’isoler pendant plusieurs jours, la durée de l’isolement varie selon le statut vaccinal du joueur (5jours d’isolement pour une personne vaccinée et 7 jours pour une personne non vaccinée).  L’isolement va augmenter le score du joueur. 

Au début du jeu, le joueur doit créer son  personnage, deux types de personnages son possible, personne vacciné et personne non vacciné. Chaque type de personnage à deux sous-types, personne fragile et personne non fragile.
Les personnes fragiles ont un indice de décès important cette indice varié selon le statut vaccinal de la personne ( __l’indice de décès__ est de __0,05__ pour __une personne vaccinée fragile__ et __0 ,1__ pour __une personne fragile non vaccinée__).

Au cours du jeux et comme dans la vraie vie __le joueur__ a à sa disposition plusieurs éléments de protections, __la vaccination, le masque chirurgical, le masque FFP2__. 
Chaque __élément de protection__ confère au joueur __un indice de contamination différent__.
Les éléments de protection sont __répartis d’une manière aléatoire sur la carte__. 

Pour se déplacer le joueur peut utiliser les commandes suivantes : 
__Z__ ( pour avancer d'une case), __Q__ ( pour aller à gauche), __S__ ( pour reculer), et __D__ (pour aller à droite). __Un déplacement fait augmenter ton score de 1__.

Enfin, __Le joueur__ pourrait __mourir s’il n’est pas guéri du COVID__.

Pour la réalisation de ce jeu on a créé :
o	__03 classes abstraites (class Masque, class Vaccine, class NonVaccine)__ 
o	__Une interface Protection__
o	__Une enumération Zone__
o	__12 classes public__ 

__Liste des class__ : 

•	Chirurgical: 
•	Covid : 
•	CovidRun
•	FFP2
•	Masques
•	Menu
•	NonVaccine
•	NonVaccineFragile
•	NonVaccineNonFragile
•	Obstacles
•	Partie
•	Personnage
•	Protection
•	Read
•	Vaccin
•	Vaccine
•	VaccineFragile
•	VaccineNonFragile
•	Zone

--------------------------------------

## le programme est pricipal est CovidRun.java.
