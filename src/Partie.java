import java.util.* ;
import java.io.* ;

public class Partie {
  Scanner sc = new Scanner(System.in) ;
  boolean vaccine ;
  boolean comm ;
  Personnage you ;

  public String lireNOM(){

    System.out.println("        Bienvenue dans COVID RUN !");
    System.out.println() ;
    System.out.println() ;

    System.out.println("    Commencons par faire connaissance...") ;

    System.out.println() ;

    System.out.println("Quel est ton petit nom ?") ;
    return (sc.nextLine());
  }


    public boolean lireAge () {
      boolean commor;
      System.out.println("As-tu plus de 70 ans ? Tape 'O' pour oui, et 'N' pour non.") ;
      String age = sc.nextLine() ;

      if ("o".equalsIgnoreCase(age)){
          commor = true ;
      }
      else{
        if ("n".equalsIgnoreCase(age)){
          commor = false ;
        }
        else{
           while ((!"o".equalsIgnoreCase(age))&&(!"n".equalsIgnoreCase(age))) {
              System.out.println("Tu as du faire une faute de frappe. Essaie 'O' ou 'N'.") ;
              age = sc.nextLine() ;
            }
            if ("o".equalsIgnoreCase(age)) commor = true ;
            else commor = false ;
        }
      }

      return commor ;

    }

    public boolean vaccination () {
      boolean vaccin = false ;
      System.out.println("Es-tu vaccine ? Tape 'O' pour oui, et 'N' pour non (Promis, on ne te jugera pas.)") ;
      String vax = sc.nextLine() ;
      if ("o".equalsIgnoreCase(vax)){
        vaccin = true ;
      }

      else{

        if ("n".equalsIgnoreCase(vax)){
          vaccin = false ;
        }

        else{
          while ((!("o".equalsIgnoreCase(vax))) && (!("n".equalsIgnoreCase(vax)))){
            System.out.println("Tu as du faire une faute de frappe. Essaie 'O' ou 'N'.") ;
            vax = sc.nextLine() ;
          }
          if ("o".equalsIgnoreCase(vax)){
            vaccin = true ;
          }
          else {
            vaccin = false ;
          }
        }
      }
      return vaccin ;
    }

    public boolean commorbidites(boolean ageCom) {
        boolean commorb = ageCom ;

        if (!commorb) {
        System.out.println("As-tu des commorbidites ? Tape 'O' pour oui, et 'N' pour non.") ;
        String com = sc.nextLine() ;
        if ("o".equalsIgnoreCase(com)){
          commorb = true ;
        }
        else{
          if ("n".equalsIgnoreCase(com)){
            commorb = false ;
          }
          else{
            while ((!"o".equalsIgnoreCase(com)) && (!"n".equalsIgnoreCase(com))){
              System.out.println("Tu as du faire une faute de frappe. Essaie 'O' ou 'N'.") ;
              com = sc.nextLine() ;
            }
            if ("o".equalsIgnoreCase(com)) commorb = true ;
            else commorb = false ;
          }
        }
      }
      return commorb ;

    }

    public Personnage construirePersonnage(){
      String nm = lireNOM ();
      boolean age = lireAge () ;
      boolean vaccine = vaccination() ;
      boolean comm = commorbidites(age);
      if ((vaccine) && (!comm)){
        you = new VaccineNonFragile(nm) ;
      }
      else{
        if ((vaccine) && (comm)) {
          you = new VaccineFragile(nm) ;
        }
        else {
          if ((!vaccine) && (comm)) {
            you = new NonVaccineFragile(nm) ;
          }
          else{
            you = new NonVaccineNonFragile(nm) ;
          }
        }
      }
      return you;
    }


    public void affichPersonnage () {
      Personnage you = construirePersonnage() ;
      System.out.println("Bravo toi ! Tu as cree ton personnage :) Le voici:") ;
      System.out.println(you.toString()) ;
      System.out.println() ;
      System.out.println("Tu te situes au depart d'une map de 20 carres sur 20.");
      System.out.println("Pour le moment, tu es sur la ligne " + you.getY() + " et sur la colonne " + you.getX() + " et dans la zone: " + you.positionZone(you.getX(),you.getY())) ;
      System.out.println("Tu dois te rendre en (" + you.Y_MAP + ", " + you.X_MAP + "), dans la zone " + you.positionZone(you.X_MAP,you.Y_MAP) + " en essayant d'eviter les endroits ou le covid regne...") ;
      System.out.println() ;
      System.out.println("Pour cela, tu dois essayer d'avoir un score minimal. Un deplacement fait augmenter ton score de 1.") ;
      System.out.println() ;
      System.out.println("Pour le moment, tu n'as pas de masque.");
      System.out.println("Tu peux donc facilement etre contamine si tu es cas contact (Probabilite de " + you.getIndiceContamination()*100 + " %).") ;
      System.out.println("Tu peux trouver des masques sur la map qui te rendront moins vulnerable.") ;
      System.out.println("Dirige toi avec les commandes Z(avancer d'une case), Q(aller a gauche), S(reculer), et D(aller a droite)") ;
    }

    public void direction (){
      char dir = sc.next().charAt(0) ;

      if (dir == 'z') {
        dir = 'Z' ;
      }
      if (dir == 's') {
        dir = 'S' ;
      }
      if (dir == 'q') {
        dir = 'Q' ;
      }
      if (dir == 'd') {
        dir = 'D' ;
      }

      switch (dir) {
        case 'Z' :
          you.avancer() ;
          break ;
        case 'S' :
          you.reculer() ;
          break ;
        case 'Q' :
          you.allerGauche() ;
          break ;
        case 'D' :
          you.allerDroite() ;
          break ;
        default:
          System.out.println("Ceci n'est pas une direction. Dirige toi avec les touches Z, Q, S ou D.") ;
          dir = sc.next().charAt(0) ;
          break ;
      }
    }

    public boolean surUnCovid (Covid covid, Personnage you) {
      return ((covid.getX() == you.getX()) && (covid.getY() == you.getY()));
    }

    public boolean attrapeCovid(Covid[] covidTab, Personnage you){
      boolean surCovid = false ;
      for (int i = 0 ; i < covidTab.length ; i++){
        if (surUnCovid(covidTab[i], you)) surCovid = true ;
      }
      return surCovid ;
    }

    public boolean surUnVaccin(Vaccin vaccin, Personnage you) {
      return ((vaccin.getX() == you.getX()) && (vaccin.getY() == you.getY()));
    }

    public boolean attrapeVaccin(Vaccin[] vaccinTab, Personnage you){
      boolean surVaccin = false ;
      for (int i = 0 ; i < vaccinTab.length ; i++){
        if (surUnVaccin(vaccinTab[i], you)) surVaccin = true ;
      }
      return surVaccin ;
    }

    public boolean surUnFFP2 (FFP2 ffp2, Personnage you) {
      return ((ffp2.getX() == you.getX()) && (ffp2.getY() == you.getY()));
    }

    public boolean attrapeFFP2(FFP2[] ffp2Tab, Personnage you){
      boolean surFFP2 = false ;
      for (int i = 0 ; i < ffp2Tab.length ; i++){
        if (surUnFFP2(ffp2Tab[i], you)) surFFP2 = true ;
      }
      return surFFP2 ;
    }

    public boolean surUnChirurgical (Chirurgical masque, Personnage you){
      return ((masque.getX() == you.getX()) && (masque.getY() == you.getY()));
    }

    public boolean attrapeChirurgical(Chirurgical[] chirurgicalTab, Personnage you){
      boolean surChirurgical = false ;
      for (int i = 0 ; i < chirurgicalTab.length ; i++){
        if (surUnChirurgical(chirurgicalTab[i], you)) surChirurgical = true ;
      }
      return surChirurgical ;
    }

    public boolean surUnContact(Covid covid, Personnage you ){
      return (((you.getX() == covid.getX()-1) && (you.getY() == covid.getY()))
              || ((you.getX() == covid.getX()+1) && (you.getY() == covid.getY()))
              || ((you.getX() == covid.getX()) && (you.getY() == covid.getY() + 1))
              || ((you.getX() == covid.getX()) && (you.getY() == covid.getY() - 1))) ;
    }

    public boolean casContact(Covid[] covidTab, Personnage you){
      boolean casContact = false ;
      for (int i = 0 ; i < covidTab.length ; i++) {
        if (surUnContact(covidTab[i], you)) casContact = true ;
      }
      return casContact ;
    }

    public void moveCovid (Covid[] covidTab){
      for (int i = 0 ; i < covidTab.length ; i++) {
        covidTab[i].move() ;
      }
    }

    public void moveVaccin(Vaccin[] vaccinTab){
      for (int i = 0 ; i < vaccinTab.length ; i++){
        vaccinTab[i].move() ;
      }
    }

    public void moveFFP2(FFP2[] ffp2Tab) {
      for (int i = 0 ; i < ffp2Tab.length ; i++){
        ffp2Tab[i].move() ;
      }
    }

    public void moveMasque (Chirurgical[] masqueTab){
      for (int i = 0 ; i < masqueTab.length ; i++){
        masqueTab[i].move() ;
      }
    }

    public void caseCovid (boolean surCovid, Personnage you) {
      if (surCovid){
        System.out.println("Zut ! Tu as attrape le covid.") ;
        System.out.println("Tu dois donc t'isoler pendant " + you.getJoursConfinement() + " jours avant de pouvoir te deplacer a nouveau.") ;
        you.incrementerScore(you.getJoursConfinement()) ;
        System.out.println("Quand tu as fini de t'isoler, appuie sur la touche F du clavier puis valide avec entree.") ;
        char isolement = sc.next().charAt(0) ;
        if (isolement=='F') {
          isolement='f';
        }
        while (isolement != 'f'){
          System.out.println("Tu as presse la mauvaise touche. Appuye sur F puis valider avec entree.");
          isolement = sc.next().charAt(0) ;
          if (isolement=='F') {
          isolement='f';
          }
        }
        Random m = new Random() ;
        int meurt = m.nextInt(100) + 1 ;
        if (meurt <= you.getIndiceDeces()*100){
          System.out.println("Malheureusement, le covid a eu raison de toi lors de ton isolement. Tu es mort.") ;
          System.out.println("En effet, tu etais considere comme fragile. Il y avait donc un risque, minime, que tu ne puisses pas survivre.") ;
          System.exit(0) ;
        }
          System.out.println() ;
      }
    }

    public void caseVaccin (boolean surVaccin, Personnage you) {
        if ((surVaccin) && (!you.getVaccine())){
        System.out.println("Vous avez l'opportunite de vous faire vacciner. Souhaitez vous le faire ? (O/N)") ;
        System.out.println("Cela vous permettra d'etre mieux immunise face au virus.");
        String choix = sc.nextLine();
        while ((!"o".equalsIgnoreCase(choix)) && (!"n".equalsIgnoreCase(choix))){
          System.out.println("Tu as du faire une faute de frappe. Essaie 'O' ou 'N'.") ;
          choix = sc.nextLine() ;
        }
        if ("o".equalsIgnoreCase(choix)){
          ((NonVaccine)you).seFaitVacciner() ;
          System.out.println("Tres bon choix ! Tu n'as maintenant plus que " + you.getIndiceContamination()*100 + "% de chances d'etre contamine si tu es cas contact.");
        }
        else {
          System.out.println("Tres bien. Le vaccin ne t'a pas ete administre.") ;
        }
        System.out.println() ;
      }
    }


    public void caseFffp (boolean surFFP2, Personnage you) {
        if ((surFFP2) && (!you.getFFP2())){
        System.out.println("Bravo ! Tu as trouve un masque FFP2. Tu est beaucoup moins vulnerable maintenant.") ;
        you.trouveFFP2();
        System.out.println() ;
      }
    }

    public void caseChirurgical (boolean surChirurgical, Personnage you) {
      if ((surChirurgical) && (!you.getChirurgical()) && (!you.getFFP2())){
        System.out.println("Bravo ! Tu as trouve un masque chirurgical. Tu est moins vulnerable maintenant.") ;
        you.trouveChirurgical();
        System.out.println() ;
      }

    }

    public void covidPositif (boolean pos) {
      if (pos){
          System.out.println("Pas de chance, tu es positif.") ;
          System.out.println("Tu dois donc t'isoler pendant " + you.getJoursConfinement() + " jours avant de pouvoir te deplacer a nouveau.") ;
          you.incrementerScore(you.getJoursConfinement()) ;
          System.out.println("Quand tu as fini de t'isoler, appuie sur la touche F du clavier puis valide avec entree.") ;
          char isolement = sc.next().charAt(0) ;
          if (isolement=='F') {
            isolement='f';
          }
          while (isolement != 'f'){
            System.out.println("Tu as presse la mauvaise touche. Appuye sur F puis valide avec entree.");
            isolement = sc.next().charAt(0) ;
            if (isolement=='F') {
              isolement='f';
            }
          }
          Random m = new Random() ;
          int meurt = m.nextInt(100) + 1 ;
          if (meurt < you.getIndiceDeces()*100){
            System.out.println("Malheureusement, le covid a eu raison de toi lors de ton isolement. Tu es mort.") ;
            System.out.println("En effet, tu etais considere comme fragile. Il y avait donc un risque, minime, que tu ne puisses pas survivre.") ;
            System.exit(0) ;
          }
        }
        else {
          System.out.println("Ton test est negatif !") ;
        }

    }

    public void caseContact (boolean contact, Personnage you ) {
        if (contact){
        System.out.println("Tu es cas contact.") ;
        System.out.println("Tu dois t'isoler pendant deux jours le temps de faire un test.") ;
        you.incrementerScore(2) ;
        System.out.println() ;
        System.out.println("Pour effectuer le test, appuie sur la touche T du clavier puis valide en appuyant sur entree.");
        boolean positif = false ;
        char test = sc.next().charAt(0) ;
          if (test=='T') {
            test = 't';
          }
        while (test != 't'){
          System.out.println("Le test n'a pas pu etre effectue. Appuyer sur T puis valider avec entree.");
          test = sc.next().charAt(0) ;
          if (test=='T') {
            test = 't';
          }
        }
        Random t = new Random() ;
        if (t.nextInt(100) + 1 <= (you.getIndiceContamination()*100)) positif = true ;
        covidPositif (positif);

        System.out.println() ;
      }

    }

    public void score (Personnage you) {

      if ((you.getX() != you.X_MAP) || (you.getY() != you.Y_MAP)){
        System.out.println("Ton score est de " + you.getScore() + " pour le moment.");
        System.out.println() ;
        System.out.println("Ou va-t-on maintenant ?") ;
      }

    }

    public void mouvement(Covid[] covidTab, Vaccin[] vaccinTab, FFP2[] ffp2Tab, Chirurgical[] masqueTab) {
      moveCovid(covidTab) ;
      moveVaccin(vaccinTab) ;
      moveFFP2(ffp2Tab) ;
      moveMasque(masqueTab) ;
    }

    public void jouer(){
      affichPersonnage() ;

      Covid[] covidTab = new Covid[15] ;
      for (int i = 0 ; i < covidTab.length ; i++){
        covidTab[i] = new Covid() ;
      }

      Vaccin[] vaccinTab = new Vaccin[9];
      for (int i = 0 ; i < vaccinTab.length ; i++){
        vaccinTab[i] = new Vaccin() ;
      }

      FFP2[] ffp2Tab = new FFP2[6];
      for (int i = 0 ; i < ffp2Tab.length ; i++){
        ffp2Tab[i] = new FFP2() ;
      }

      Chirurgical[] masqueTab = new Chirurgical[12];
      for (int i = 0 ; i < masqueTab.length ; i++){
        masqueTab[i] = new Chirurgical() ;
      }

      while ((you.getX() != you.X_MAP) || (you.getY() != you.Y_MAP)){
        direction () ;

        System.out.println("Tu te situes desormais en (" + you.getX() + ", " + you.getY() +") et dans la zone: " + you.positionZone(you.getX(),you.getY()) + ".") ;

        boolean surCovid = attrapeCovid (covidTab, you) ;
        boolean surVaccin = attrapeVaccin (vaccinTab, you);
        boolean surFFP2 = attrapeFFP2 (ffp2Tab, you);
        boolean surChirurgical = attrapeChirurgical(masqueTab, you);
        boolean contact = casContact(covidTab, you) ;
        caseCovid(surCovid, you);
        caseVaccin(surVaccin, you);
        caseFffp(surFFP2, you) ;
        caseChirurgical(surChirurgical, you) ;
        caseContact(contact, you ) ;
        score(you);
        mouvement(covidTab, vaccinTab, ffp2Tab, masqueTab) ;
      }
      System.out.println("Bravo, tu es arrive au point de rendez-vous en seulement " + you.getScore() + " jours !") ;
  }
}
