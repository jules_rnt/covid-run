public class Personnage {

  /*
  *ATTRIBUTS DES PERSONNAGES
  */

  private final String nom ;
  private int score, X, Y, nbJoursConfinement;
  private double indiceContamination, indiceDeces ;
  private final boolean  contamine, casContact;
  private boolean vaccine, commorbidites, ffp2, chirurgical;
  private Zone position ;

  //Attributs statiques: caractéristiques de la map (on considere une map de 20 sur 20)

  public static int X_MAP = 20 ;
  public static int Y_MAP = 20 ;

  //CONSTRUCTEUR DE PERSONNNAGE

  public Personnage(String name){
    nom = name ;
    score = 0 ;
    X = 1 ;
    Y = 1 ;
    contamine = false ;
    casContact = false ;
    ffp2 = false ;
    chirurgical = false ;
  }

  //METHODES

  public void avancer(){
    if (Y < Y_MAP){
      Y+=1;
      score += 1 ;
    }
    else {
      System.out.println("Vous ne pouvez plus avancer, vous allez sortir de la map !") ;
    }
  }

  public void reculer(){
    if (Y > 1){
      Y -= 1 ;
      score += 1;
    }
    else{
      System.out.println("Vous ne pouvez plus reculer, vous allez sortir de la map !") ;
    }
  }

  public void allerDroite(){
    if (X < X_MAP) {
      X+=1;
      score += 1;
    }
    else{
      System.out.println("Vous ne pouvez plus aller a droite, vous allez sortir de la map !") ;
    }
  }

  public void allerGauche(){
    if (X > 1) {
      X-=1;
      score += 1;
    }
    else{
      System.out.println("Vous ne pouvez plus aller a gauche, vous allez sortir de la map !") ;
    }
  }

  public void trouveFFP2(){
    this.setFFP2(true);
    this.setIndice(this.getIndiceContamination()-0.2);
    System.out.println("Tu as maintenant " + Math.round(this.getIndiceContamination()*100) + "% de chances d'etre contamine si tu deviens cas contact.");
  }

  public void trouveChirurgical(){
    this.setChirurgical(true);
    this.setIndice(this.getIndiceContamination()-0.1);
    System.out.println("Tu as maintenant " + Math.round(this.getIndiceContamination()*100) + "% de chances d'etre contamine si tu deviens cas contact.");
  }

  public Zone positionZone (int x, int y) {
    if ((x<X_MAP/2) && (y<Y_MAP/2)){
      position = Zone.SUD_WEST ;
    }
    if ((x<X_MAP/2) && (y>Y_MAP/2)){
      position = Zone.NORD_WEST ;
    }
    if ((x>X_MAP/2) && (y>Y_MAP/2)){
      position = Zone.NORD_EST ;
    }
    if ((x>X_MAP/2) && (y<Y_MAP/2)){
      position= Zone.SUD_EST ;
    }

    return position ;
  }

  public void setJoursConfinements(int N){
    nbJoursConfinement = N ;
  }

  public void setIndice(double i){
    indiceContamination = i ;
  }

  public void setIndiceDeces(double d){
    indiceDeces = d ;
  }

  public void setCommorbidites(boolean C){
    commorbidites =  C ;
  }

  public void setVaccine(boolean v){
    vaccine = v ;
  }

  public void setChirurgical(boolean c){
    chirurgical = c ;
  }

  public void setFFP2(boolean f){
    ffp2 = f ;
  }

  public void incrementerScore(int n){
    score+=n ;
  }

  public String getNom(){return nom ;}
  public int getX(){ return X ;}
  public int getY(){return Y ;}
  public int getScore(){return score;}
  public int getJoursConfinement(){return nbJoursConfinement ;}
  public double getIndiceContamination() {return indiceContamination ;}
  public boolean getVaccine(){return vaccine ;}
  public boolean getContamine(){return contamine ;}
  public boolean getCommorbidites() {return commorbidites ;}
  public boolean getCasContact() {return casContact ;}
  public boolean getFFP2() {return ffp2 ;}
  public boolean getChirurgical() {return chirurgical ;}
  public double getIndiceDeces(){ return indiceDeces ;}

  public String toString() {
    return ("ton nom:  " + nom + "\n") ;
  }

}
