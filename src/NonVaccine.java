public abstract class NonVaccine extends Personnage implements Protection{

  public NonVaccine(String nom){
    super(nom) ;
    this.setJoursConfinements(7) ;
    this.setIndice(0.5) ;
    this.setVaccine(false) ;
  }

  public void seFaitVacciner(){
    this.setVaccine(true) ;
    this.setJoursConfinements(5) ;
    this.setIndice(0.4) ;
    if (this.getChirurgical()) this.setIndice(0.3) ;
    if (this.getFFP2()) this.setIndice(0.2) ;
  }

  public String toString(){
    return super.toString() + "Tu n'es pas vaccine.\n" ;
  }


}
