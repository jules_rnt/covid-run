public abstract class Vaccine extends Personnage {

  public Vaccine(String nom){
    super(nom);
    this.setJoursConfinements(5) ;
    this.setIndice(0.4) ;
    this.setVaccine(true) ;
  }

  //To String
  public String toString(){
    return super.toString() + "Tu es vaccine.\n" ;
  }

}
