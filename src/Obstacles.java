import java.util.Random ;

public class Obstacles {

  private int x, y ;

  public Obstacles(){
    Random rx = new Random() ;
    this.x = rx.nextInt(19) + 1 ;
    Random ry = new Random() ;
    this.y = ry.nextInt(19) + 1 ;
  }

  public void move() {
    Random rx = new Random() ;
    this.x = rx.nextInt(19) + 1 ;
    Random ry = new Random() ;
    this.y = ry.nextInt(19) + 1 ;
  }

  public int getX() {return x ;}
  public int getY() {return y ;}

}
