import java.io.*;
import java.util.Scanner;
import java.lang.AutoCloseable;

public class Read
{
    private final String nom ;

public Read (String fichier)
{

    nom = fichier ;

}

public String getNom() {
    return nom ;
} 

public static void afficher ( String fich) throws IOException {

    Read name = new Read (fich) ;

        String line = null;
        FileReader fichierLu = new FileReader(name.getNom());
        BufferedReader bufferedReader = new BufferedReader(fichierLu);
    try
    {

        while((line = bufferedReader.readLine()) != null)
        {
            System.out.println(line);
        }

    }
    catch(IOException ex)
    {
        System.out.println("Error reading file named '" + name.getNom() + "'");
    }finally {
        fichierLu.close ();
        bufferedReader.close () ;
    }
}

}
