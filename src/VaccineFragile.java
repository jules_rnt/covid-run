public class VaccineFragile extends Vaccine {

  public VaccineFragile(String nom){
    super(nom);
    this.setCommorbidites(true) ;
    this.setIndiceDeces(0.05) ;
  }

  public String toString(){
    return super.toString() + "Tu es considere comme fragile.\n" ;
  }
}
