public class NonVaccineFragile extends NonVaccine implements Protection {

  public NonVaccineFragile(String nom){
    super(nom);
    this.setCommorbidites(true) ;
    this.setIndiceDeces(0.1);
  }

  public String toString(){
    return super.toString() + "Tu es considere comme fragile.\n" ;
  }
}
