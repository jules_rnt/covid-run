public class VaccineNonFragile extends Vaccine {

  public VaccineNonFragile(String nom){
    super(nom);
    this.setCommorbidites(false) ;
    this.setIndiceDeces(0) ;
  }

  public String toString(){
    return super.toString() + "Tu n'es pas considere comme fragile.\n" ;
  }
}
