import java.util.Scanner ;
import java.io.*;

  public class Menu  {


      Scanner sc = new Scanner(System.in) ;

      public void affich () {
        System.out.println("\n \n        Bienvenue dans COVID RUN !\n") ;
        System.out.println ("choisir une option :") ;
        System.out.println ("          1 - jouer ");
        System.out.println ("          2 - connaitre les regles du jeu");
        System.out.println ("          0 - EXIT ?\n");
      }

      public static void programmeContinue(Scanner sc){
        Menu mm = new Menu () ;
        mm.affich();
        int choix = 0;
        try {
          choix = sc.nextInt();
        } catch (Exception e){
          System.out.println ("choix non reconnu");
        }

      switch (choix) {

        case 1:
          Partie p = new Partie() ;
          p.jouer() ;
          break;

        case 2:
          String nomFichier = "../doc/text1.txt";
          Read fich = new Read (nomFichier) ;
          try {
            fich.afficher(nomFichier) ;
          }
          catch(IOException e) {
                    e.printStackTrace();
          }
          break;

        case 0 :
          System.out.println("EXIT");
          sc.close();
          System.exit(0);
          break;

        default:
          System.out.println ("choix non reconnu");
          break;
        }
    }

  public void stopProgramme (Scanner sc) {


      System.out.println ("Voulez-vous continuer à jouer ?") ;

      System.out.println ("          1 - Oui, Continuer le jeu ");
      System.out.println ("          0 - Non, quitter le jeu ");

        int reponse = 0;
        try {
          reponse = sc.nextInt();
        } catch (Exception e){
          System.out.println ("choix non reconnu");
        }
      // on controle que la reponse soit == 1 ou == 2

      while ((reponse!=1)&&(reponse!=0)){
        System.out.println ("merci de répondre par : 0 pour quitter le jeu ou par de 1 pour continuer le jeu ") ;
        reponse = sc.nextInt ();
      }

       while (reponse ==1) {
        programmeContinue(sc);
      }

      System.out.println("EXIT");
      sc.close();
      System.exit(0);
  }

}
